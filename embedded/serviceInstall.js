var installer = require('strong-service-install');
console.log("Installing");
var opts = {
    name: 'nodebuggy',
    author: 'Chris Woodle',
    description: 'NodeBuggy Service',
    user: 'root',
    command: 'sudo node core.js',
    cwd: '/home/pi/nodebuggy/embedded/',
    systemd: true,
    force: true
};
installer(opts, function (err, result) {
    if (err) {
        console.error('Failed to install service:', err.message);
        process.exit(1);
    } else {
        console.log('Successfully installed service:', result);
        process.exit(0);
    }
});