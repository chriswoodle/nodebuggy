'use strict';
//var serverIP = "192.168.11.129";
var serverIP = "163.118.161.99";
var serverPort = "5858";
var socket;

var serialport = require('serialport');
var SerialPort = serialport.SerialPort;

// list serial ports:
serialport.list(function (err, ports) {
    ports.forEach(function (port) {
        console.log(port.comName);
    });
});

var myPort = new SerialPort("/dev/ttyAMA0", {
    baudRate: 115200,
    parser: serialport.parsers.readline('\n')

});

try {
    socket.disconnect();
}
catch (err) { }

socket = require('socket.io-client')('http://' + serverIP + ':' + serverPort);

var serialPortInUse = false;

var throttleValue = 0;
var throttleDirection = "F";
var directionValue = 0;
var directionSign = "R";

var timeout;

socket.on('control-data', function (data) {
    //console.log(data.throttle.value + ": " + data.throttle.sign);
    //console.log(data.direction.value + ": " + data.direction.sign);

    throttleValue = data.throttle.value;
    throttleDirection = data.throttle.sign;
    directionValue = data.direction.value;
    directionSign = data.direction.sign;
    clearTimeout(timeout);
    timeout = setTimeout(function () {
        throttleValue = 0;
        throttleDirection = "F";
        directionValue = 0;
        directionSign = "R";

    },1000);
});

myPort.on("open", function () {
    console.log("serial init");
    setInterval(function () {
        if (myPort.isOpen()) {
            var serialData = "T" + throttleDirection + throttleValue + ";" + "D" + directionSign + directionValue + ";";
            console.log(serialData);
            myPort.write(serialData);
        }
        }, 50)
    
              
       
});

myPort.on("error", function (err) {
    console.log("Error:" + err);
});
myPort.on("data", function (d) {
    console.log("Serial data!: " + d);
});
socket.on('connect', function () { console.log('Socket connected!'); });


socket.on('error', function (data) { console.log("ERROR: " + data); });
socket.on('disconnected', function () { console.log('Socket disconnected!'); });

process.on('SIGINT', function () {
    console.log("Caught interrupt signal");
    myPort.close();
    process.exit();
});