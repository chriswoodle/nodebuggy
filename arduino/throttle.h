#include <Servo.h>

#define THROTTLE_MAXREVERSE  1000
#define THROTTLE_MAXFORWARD  2000
#define THROTTLE_IDLE        1500
#define THROTTLEPIN          10

Servo throttle;
int throttle_pulse;

void throttle_init();
void throttle_set(int pulse);
void throttle_forwardPct(int pct);
void throttle_reversePct(int pct);
void throttle_idle();
void throttle_stop();
