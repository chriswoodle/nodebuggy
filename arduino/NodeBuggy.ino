#include <Servo.h>

#define THROTTLE_MAXREVERSE		1000
#define THROTTLE_MAXFORWARD		2000
#define THROTTLE_IDLE			1500
#define THROTTLEPIN				10

#define MAXLEFT					2000
#define MAXRIGHT				1000
#define MIDDLE					1500
#define DIRPIN					9

#define RX_THROTTLE_MODE		'T'
#define RX_THROTTLE_FORWARD		'F'
#define RX_THROTTLE_REVERSE		'R'

#define RX_DIRECTION_MODE		'D'
#define RX_DIRECTION_LEFT		'L'
#define RX_DIRECTION_RIGHT		'R'

#define TIMEOUT					1000
Servo throttle;
int throttle_pulse;
#pragma region throttle
void throttle_init() {
	throttle.attach(THROTTLEPIN);

	// ESC init sequence
	throttle_set(THROTTLE_MAXFORWARD);
	delay(25);
	throttle_set(THROTTLE_MAXREVERSE);
	delay(25);
	throttle_set(THROTTLE_IDLE);

	throttle_pulse = THROTTLE_IDLE;
}

void throttle_set(int pulse) {
	throttle.writeMicroseconds(pulse);
	throttle_pulse = pulse;
}

void throttle_forwardPct(int pct) {
	int newPulse = int(double(THROTTLE_IDLE + (double(abs(THROTTLE_IDLE - THROTTLE_MAXFORWARD))*(double(pct) / 100))));

	throttle.writeMicroseconds(newPulse);
	throttle_pulse = newPulse;
}

void throttle_reversePct(int pct) {
	int newPulse = int(double(THROTTLE_IDLE - (double(abs(THROTTLE_IDLE - THROTTLE_MAXREVERSE))*(double(pct) / 100))));

	if (throttle_pulse >= THROTTLE_IDLE) {
		throttle_idle();
		delay(100);
		throttle.writeMicroseconds(newPulse);
		delay(100);
		throttle_idle();
		delay(100);
	}
	throttle.writeMicroseconds(newPulse);
	throttle_pulse = newPulse;
}

void throttle_idle() {
	throttle.writeMicroseconds(THROTTLE_IDLE);
	throttle_pulse = THROTTLE_IDLE;
}

void throttle_stop() {
	/*
	int i;
	if (throttle_pulse > THROTTLE_IDLE) {


	  for (i=throttle_pulse; i > THROTTLE_IDLE; i-=10) {
		throttle_set (i);
		delay (20);
	  }
	}
	else if (throttle_pulse < THROTTLE_IDLE) {
	  for (i=throttle_pulse; i < THROTTLE_IDLE; i+=10) {
		throttle_set (i);
		delay (20);
	  }
	}
	*/
	throttle.writeMicroseconds(THROTTLE_IDLE);
	throttle_pulse = THROTTLE_IDLE;
}
#pragma endregion

Servo dir;
#pragma region direction

void dir_init() {
	dir.attach(DIRPIN);
}

void dir_set(int pulse) {
	dir.writeMicroseconds(pulse);
}

void dir_leftPct(int pct) {
	dir.writeMicroseconds(int(double(MIDDLE + (double(abs(MIDDLE - MAXLEFT))*(double(pct) / 100)))));
}

void dir_rightPct(int pct) {
	dir.writeMicroseconds(int(double(MIDDLE - (double(abs(MIDDLE - MAXRIGHT))*(double(pct) / 100)))));
}

void dir_straight() {
	dir.writeMicroseconds(MIDDLE);
}
#pragma endregion

unsigned long previousMillis = 0;
char RXMode;
char RXModeModifier;
String payload = "";

void setup()
{
	dir_init();
	throttle_init();
	Serial.begin(115200);
	Serial.println("Arduino start");
}

void loop()
{
	unsigned long currentMillis = millis();
	if (Serial.available()) {
		char byteIn = Serial.read();
		if (byteIn == RX_THROTTLE_MODE || byteIn == RX_DIRECTION_MODE) {
			RXMode = byteIn;
			// clears previous input for broken packets
			payload = "";
			RXModeModifier = '!';
		}
		else {
			if (byteIn != ';') {
				if (RXMode == RX_THROTTLE_MODE && (byteIn == RX_THROTTLE_FORWARD || byteIn == RX_THROTTLE_REVERSE)) {
					RXModeModifier = byteIn;
				}
				else {
					if (RXMode == RX_DIRECTION_MODE && (byteIn == RX_DIRECTION_LEFT || byteIn == RX_DIRECTION_RIGHT)) {
						RXModeModifier = byteIn;
					}
					else {
						payload += byteIn;
					}
				}
			}
			else {
				/*
				Serial.print("Mode:");
				Serial.println(RXMode);
				Serial.print("Modifier:");
				Serial.println(RXModeModifier);
				Serial.print("Value: ");
				Serial.println(payload.toInt());
				*/
				if (RXModeModifier != '!') {
					if (RXMode == RX_THROTTLE_MODE) {
						if (RXModeModifier == RX_THROTTLE_FORWARD) {
							throttle_forwardPct(payload.toInt());
						}
						if (RXModeModifier == RX_THROTTLE_REVERSE) {
							throttle_reversePct(payload.toInt());
						}
					}
					else {
						if (RXMode == RX_DIRECTION_MODE) {
							if (RXModeModifier == RX_DIRECTION_LEFT) {
								dir_leftPct(payload.toInt());
							}
							if (RXModeModifier == RX_DIRECTION_RIGHT) {
								dir_rightPct(payload.toInt());
							}
						}
					}
				}

				// clear the data for new input:
				payload = "";
				RXModeModifier = '!';
				// Reset Timeout
				previousMillis = currentMillis;
			}
		}
	}
	// Safety timeout for connection loss
	if (currentMillis - previousMillis >= TIMEOUT) {
		previousMillis = currentMillis;
		throttle_stop();
		dir_straight();
	}
	delay(5);
}



