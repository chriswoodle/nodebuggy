'use strict'
var io = require('socket.io').listen(5858);
var gamepad = require("gamepad");

// Axis
var XBOXONE_LEFT_JOYSTICK_HORIZONTAL = 0;
var XBOXONE_LEFT_JOYSTICK_VERTICAL = 1;
var XBOXONE_RIGHT_JOYSTICK_HORIZONTAL = 2;
var XBOXONE_RIGHT_JOYSTICK_VERTICAL = 3;

var XBOXONE_RIGHT_TRIGGER = 5;
var XBOXONE_LEFT_TRIGGER = 4;
// button numbers
var XBOXONE_DPAD_UP = 0;
var XBOXONE_DPAD_DOWN = 1;
var XBOXONE_DPAD_LEFT = 2;
var XBOXONE_DPAD_RIGHT = 3;
var XBOXONE_MENU_BUTTON = 4;
var XBOXONE_VIEW_BUTTON = 5;

var XBOXONE_LEFT_BUMPER = 8;
var XBOXONE_RIGHT_BUMPER = 9;

var XBOXONE_A_BUTTON = 10;
var XBOXONE_B_BUTTON = 11;
var XBOXONE_X_BUTTON = 12;
var XBOXONE_Y_BUTTON = 13;


var throttleValue = 0;
var throttleDirection;
var directionValue = 0;
var directionSign;

io.sockets.on("connection", function (socket) {
    console.log('Socket.io connected.');
    var stream = setInterval(function () {

        var data = {
            'throttle': {
                'value': throttleValue,
                'sign': throttleDirection
            },
            'direction': {
                'value': directionValue,
                'sign': directionSign
            }
        };
        console.log(data);
        //console.log(data);
        io.emit("control-data", data);

    }, 50);

    socket.on("disconnect", function () {
        console.log('Socket.io disconnected.');
        clearInterval(stream);
    });
});






// Initialize the library 
gamepad.init()

// List the state of all currently attached devices 
for (var i = 0, l = gamepad.numDevices() ; i < l; i++) {
    console.log(i, gamepad.deviceAtIndex());
}

// Create a game loop and poll for events 
setInterval(gamepad.processEvents, 16);
// Scan for new gamepads as a slower rate 
setInterval(gamepad.detectDevices, 500);

// Listen for move events on all gamepads 
gamepad.on("move", function (id, axis, value) {
    if (axis == XBOXONE_LEFT_JOYSTICK_HORIZONTAL) {
        var rawValue = Math.round((value) * 100);
        if (rawValue >= 0) {
            directionSign = "L";
        }
        else {
            directionSign = "R"
        }
        directionValue = Math.abs(rawValue);
        /*console.log("move", {
            id: id,
            axis: axis,
            value: value,
        });*/
    }
    if (axis == XBOXONE_RIGHT_TRIGGER || axis == XBOXONE_LEFT_TRIGGER) {
        throttleValue = Math.round((value + 1) * 50);
    }
    if (axis == XBOXONE_RIGHT_TRIGGER) {
        throttleDirection = "F";
    }
    if (axis == XBOXONE_LEFT_TRIGGER) {
        throttleDirection = "R";
    }
});

// Listen for button up events on all gamepads 
gamepad.on("up", function (id, num) {
    console.log("up", {
        id: id,
        num: num,
    });
});

// Listen for button down events on all gamepads 
gamepad.on("down", function (id, num) {
    console.log("down", {
        id: id,
        num: num,
    });
});